# Web UI
1. Main goal
  * Web UI for PAROTA project
      * display rota
      * edit rota
      * generate new rota
      * change shift
      * vacation request
      * export rota to .csv
2. Prerequisites
  * Java Script

How to setup docker:
1. Install virtualbox and docker-machine
2. Create a new machine called "parota"
```
docker-machine create --driver virtualbox parota
```
3. Change mounted directory:
 * Shutdown parota machine `docker-machine stop parota`
 * Open Virtualbox, choose parota machine, go to Settings, Shared Folders
 * Remove default mounted directory
 * Add main project directory (the directory containing all subprojects: web-ui, server)
 * Don't forget to tick the 'Auto-mount' option
 * Start machine again `docker-machine start parota`
 * Check if directory was mounted properly
 ```
 docker-machine ssh parota
 ls /
 ```

How to setup web-ui:
1. Ssh into parota machine `docker-machine ssh parota`
2. Install `docker-compose` using the following commands:
    `url="https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)";
     docker_compose_dest="/usr/local/bin/docker-compose";
     sudo curl -L ${url} -o ${docker_compose_dest};
     sudo chmod +x ${docker_compose_dest};`
3. Install project dependencies `docker-compose run --rm devtools bower --allow-root install` and wait for finish
4. Run nginx (server) container `docker-compose up -d nginx`
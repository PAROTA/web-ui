(function() {
    'use strict';

    angular
        .module('parota')
        .run(runBlock);

    runBlock.$inject = ['$rootScope', '$location', '$route', 'authenticationService', 'notificationService'];

    function runBlock($rootScope, $location, $route, authenticationService, notificationService) {
        const publicAccessPages = ['/login', '/'];

        $rootScope.$on('$routeChangeStart', (event, next, current) => {
            if(authenticationService.isAuthenticated()) {
                if(userDataNotLoaded()) {
                    event.preventDefault();
                    loadUserData();
                }
            }
            else {
                handleRestrictedPages(next.$$route.originalPath);
            }
        });

        function userDataNotLoaded() {
            return _.isEmpty(authenticationService.getUserData());
        }

        function loadUserData() {
            return authenticationService.getSessionUser().then((response) => {
                authenticationService.setUserData(response.data);
                tryToInitializeWebSockets();
                $route.reload();
            });
        }

        function handleRestrictedPages(targetUrl) {
            if(triesToAccessRestrictedPage(targetUrl)) {
                event.preventDefault();
                $location.path('/login');
            }

            function triesToAccessRestrictedPage(targetUrl) {
                return !(_.includes(publicAccessPages, targetUrl));
            }
        }

        function tryToInitializeWebSockets() {
            if(!notificationService.getSocketState().notificationSocketInitialized) {
                notificationService.initWebSockets(authenticationService.getUserData().id);
            }
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('parota')
        .directive('ngEnter', ngEnter);

    function ngEnter() {
        var directive = {
            link: link
        };

        return directive;
        /////////////////

        function link(scope, elem, attrs) {
            elem.bind('keydown keypress', (event) => {
                if(event.which === 13) {
                    scope.$apply(() => scope.$eval(attrs.ngEnter));
                    event.preventDefault();
                }
            });
        }
    }
})();
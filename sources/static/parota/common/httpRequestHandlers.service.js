(function() {
    'use strict';

    angular
        .module('parota')
        .factory('httpRequestHandlers', httpRequestHandlers);

    httpRequestHandlers.$inject = ['$q', 'messageService'];

    function httpRequestHandlers($q, messageService) {

        var service = {
            handleResponse: handleResponse,
            handleError: handleError
        };

        return service;
        ////////////////

        function handleResponse({msg, shouldDisplayMessage, shouldClearMessages} = {}) {
            return function(response) {
                if(msg) {
                    console.log(msg, response);
                }
                if(shouldDisplayMessage) {
                    messageService.displayInfo(msg);
                }
                if(shouldClearMessages) {
                    messageService.clearAllMessages();
                }

                return response;
            };
        }

        function handleError({msg, shouldDisplayMessage, shouldClearMessages} = {}) {
            return function(error) {
                if(msg) {
                    console.error(msg, error);
                }
                if(shouldDisplayMessage) {
                    messageService.displayError(msg);
                }
                if(shouldClearMessages) {
                    messageService.clearAllMessages();
                }

                return $q.reject(error);
            };
        }
    }
})();
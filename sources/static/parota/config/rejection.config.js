(function() {
    'use strict';

    angular
        .module('parota')
        .constant('rejectionConfig', rejectionConfig);

    rejectionConfig.$inject = ['$qProvider'];

    function rejectionConfig($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }
})();
(function() {
    'use strict';

    angular
        .module('parota')
        .constant('routerConfig', routerConfig);

    routerConfig.$inject = ['$routeProvider'];

    function routerConfig($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'static/parota/main/main.view.html'
            })

            .when('/login', {
                templateUrl: 'static/parota/login/login.view.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })

            .when('/search/:query?', {
                templateUrl: 'static/parota/search/search.view.html',
                controller: 'SearchController',
                controllerAs: 'vm',
                resolve: {
                    query: getSearchQuery,
                    searchResults: getSearchResults
                }
            })

            .when('/error', {
                templateUrl: 'static/parota/common/error.view.html'
            })

            .when('/rota/:id?', {
                templateUrl: 'static/parota/rota/rota.view.html',
                controller: 'RotaController',
                controllerAs: 'vm',
                resolve: {
                    targetUser: getTargetUser
                }
            })

            .when('/shifts', {
                templateUrl: 'static/parota/shifts/all/allShifts.view.html',
                controller: 'ShiftsController',
                controllerAs: 'vm'
            })

            .when('/constraints', {
                templateUrl: 'static/parota/shifts/constraints/constraints.view.html',
                controller: 'ConstraintsController',
                controllerAs: 'vm',
                resolve: {
                    allConstraints: getAllConstraints,
                    allRoles: getAllUserRoles,
                    allShiftTypes: getAllShiftTypes
                }
            })

            .when('/teams', {
                templateUrl: 'static/parota/team/team.view.html',
                controller: 'TeamController',
                controllerAs: 'vm',
                resolve: {
                    allTeams: getAllTeams
                }
            })

            .when('/users', {
                templateUrl: 'static/parota/user/list/userList.view.html',
                controller: 'UserListController',
                controllerAs: 'vm',
                resolve: {
                    allUsers: getAllUsers
                }
            })

            .when('/profile/:id?', {
                templateUrl: 'static/parota/user/profile/profile.view.html',
                controller: 'ProfileController',
                controllerAs: 'vm',
                resolve: {
                    user: getTargetUser
                }
            })

            .when('/notifications', {
                templateUrl: 'static/parota/notification/all/allNotifications.view.html',
                controller: 'AllNotificationsController',
                controllerAs: 'vm',
                resolve: {
                    targetUser: getTargetUser
                }
            })

            .when('/admin', {
                templateUrl: 'static/parota/admin/admin.view.html',
                controller: 'AdminController',
                controllerAs: 'vm'
            })

            .otherwise({
                redirectTo: '/'
            });
    }


    function getSearchQuery($route) {
        return $route.current.params.query || "";
    }

    function getSearchResults($route, $location, searchService) {
        return searchService.getData(getSearchQuery($route)).then((response) => {
            return response.data;
        }).catch((error) => {
            $location.path('/error');
        });
    }

    function getTargetUser($route, $location, authenticationService) {
        var userId = $route.current.params.id;
        if(userId) {
            return authenticationService.getUserById(userId).then((response) => {
                return response.data;
            }).catch((error) => {
                $location.path('/error');
            });
        }
        else {
            return authenticationService.getUserData();
        }
    }

    function getAllTeams(teamService) {
        return teamService.getAll().then((response) => {
            return response.data;
        });
    }

    function getAllUsers(userService) {
        return userService.getAll().then((response) => {
            return response.data;
        });
    }

    function getAllUserRoles(userRoleService) {
        return userRoleService.getAll().then((response) => {
            return response.data;
        });
    }

    function getAllConstraints(constraintsService) {
        return constraintsService.getConstraints().then((response) => {
            return response.data;
        });
    }

    function getAllShiftTypes(shiftTypeService) {
        return shiftTypeService.getAllWorkTypes().then((response) => {
            return response.data;
        });
    }
})();
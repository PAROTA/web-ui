(function(){
  'use strict';

  angular
      .module('parota')
      .config(config);

  config.$inject = ['$routeProvider', 'routerConfig',
                    '$locationProvider', 'locationProviderConfig',
                    'growlProvider', 'growlConfig',
                    'rejectionConfig', '$qProvider'];

  function config($routeProvider, routerConfig,
                  $locationProvider, locationProviderConfig,
                  growlProvider, growlConfig,
                  rejectionConfig, $qProvider) {
    routerConfig($routeProvider);
    locationProviderConfig($locationProvider);
    growlConfig(growlProvider);
    rejectionConfig($qProvider);
  }
})();

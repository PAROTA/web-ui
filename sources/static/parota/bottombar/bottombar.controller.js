(function() {
    'use strict';

    angular
        .module('parota.bottombar')
        .controller('BottomBarController', BottomBarController);

    BottomBarController.$inject = ['rotaService', 'authenticationService'];

    function BottomBarController(rotaService, authenticationService) {
        var vm = this;

        var currentWeekYear = {};

        vm.user = {};
        vm.shifts = [];
        vm.years = [];
        vm.months = [];

        vm.getPreviousWeek = getPreviousWeek;
        vm.getNextWeek = getNextWeek;

        init();
        ///////////////////


        function init() {
            vm.user = authenticationService.getUserData();
            currentWeekYear = moment();
            loadShifts();
        }

        function getPreviousWeek() {
            currentWeekYear.subtract(1, 'w');
            loadShifts();
        }

        function getNextWeek(){
            currentWeekYear.add(1, 'w');
            loadShifts();
        }

        function loadShifts() {
            rotaService.getShifts(vm.user.id, currentWeekYear.week(), currentWeekYear.year()).then((response) => {
                vm.shifts = response.data;
                vm.shifts.forEach((shift) => {
                    var currentDate = moment(shift.shiftId.date, 'DD/MM/YYYY');

                    shift.shiftId.day = currentDate.format('DD');
                    shift.shiftId.month = currentDate.format('MMMM');
                    shift.shiftId.year = currentDate.format('YYYY');
                    shift.shiftId.dayOfWeek = currentDate.format('ddd');
                });

                vm.years = countOccurrences(vm.shifts, 'shiftId.year');
                vm.months = countOccurrences(vm.shifts, 'shiftId.month');
            });
        }

        function countOccurrences(arr, groupStatement) {
            return _.map(_.groupBy(arr, groupStatement), function (value, key) { return {"value": key, "count" : value.length}; });
        }
    }
})();
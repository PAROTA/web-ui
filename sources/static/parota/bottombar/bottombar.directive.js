(function() {
    'use strict';

    angular
        .module('parota.bottombar')
        .directive('bottomBar', bottomBar);

    function bottomBar() {
        var BOTTOM_BAR_HEIGHT = '30%',
            BOTTOM_BAR_BTN_POS = 'Calc(30% - 30px)',
            HIDDEN = '30px',
            UP_ARROW = 'glyphicon glyphicon-chevron-up',
            DOWN_ARROW = 'glyphicon glyphicon-chevron-down';

        var directive = {
            link: link,
            templateUrl: 'static/parota/bottombar/bottombar.view.html',
            controller: 'BottomBarController',
            controllerAs: 'bbctrl',
            restrict: 'E'
        };

        return directive;
        /////////////////

        function link(scope, elem, attrs) {
            $('#bottom-bar-toggle-btn').data('slidedOut', false);

            $('#bottom-bar-toggle-btn').on('click', function() {
                var isVisible = $(this).data('slidedOut');

                $('#bottom-bar').css('height', isVisible ? HIDDEN : BOTTOM_BAR_HEIGHT);
                $('.bottom-bar-button-container').css('bottom', isVisible ? HIDDEN : BOTTOM_BAR_BTN_POS);
                $('#bottom-bar-toggle-btn').attr('class', isVisible ? UP_ARROW : DOWN_ARROW);

                $(this).data('slidedOut', !isVisible);
            });
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('parota.admin')
        .controller('AdminController', AdminController);

    AdminController.$inject = ['adminService', '$timeout', '$location'];

    function AdminController(adminService, $timeout, $location) {
        var vm = this;

        vm.offset = 0;

        vm.generateRota = generateRota;
        vm.downloadExcel = downloadExcel;

        init();
        /////////

        function init() {
            $timeout(() => initDatePickers());
        }

        function initDatePickers() {
            var options = {
                locale: 'pl',
                format: 'DD/MM/YYYY'
            };

            $('#rota-gen-dp').datetimepicker(options);
            $('#export-dp').datetimepicker(options);
        }

        function generateRota() {
            var startDate = $('#rota-gen-dp').data('DateTimePicker').date();
            adminService.generateRota(startDate.week(), startDate.year(), vm.offset).then((response) => {
                $location.path('/shifts');
            });
        }

        function downloadExcel() {
            var exportDate = $('#export-dp').data('DateTimePicker').date();

            window.open('/api/shifts/export/' + exportDate.format('DD') + '-' + exportDate.format('MM') + '-' + exportDate.format('YYYY'), '_blank');
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('parota.admin')
        .factory('adminService', adminService);

    adminService.$inject = ['$http', 'httpRequestHandlers'];

    function adminService($http, httpRequestHandlers) {
        var service = {
            generateRota: generateRota
        };

        return service;
        ///////////////

        function generateRota(week, year, offset) {
            return $http.post('/api/rota/generate/' + week + '-' + year + '/' + offset)
                .then(httpRequestHandlers.handleResponse({msg: 'Rota was generated! Please refresh view.', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while generating rota', shouldDisplayMessage: true}));
        }
    }

})();
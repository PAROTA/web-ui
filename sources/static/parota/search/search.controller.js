(function() {
    'use strict';

    angular
        .module('parota.search')
        .controller('SearchController', SearchController);

    SearchController.$inject = ['query', 'searchResults', 'NgTableParams', '$location'];

    function SearchController(query, searchResults, NgTableParams, $location) {
        var vm = this;

        vm.query = query;
        vm.searchResultsTable = {};

        vm.queryNotEmpty = queryNotEmpty;
        vm.applyCustomFilter = applyCustomFilter;
        vm.showRotaForUser = showRotaForUser;
        vm.isAdmin = isAdmin;
        vm.showActionsForAdmin = showActionsForAdmin;

        init();
        ///////////////

        function init() {
            initSearchResultsTable();
            initDateTimePicker();
        }

        function initSearchResultsTable() {
            var initialParameters = {
                count: 9
            };
            var settings = {
                data: mapSearchResults(searchResults),
                counts: []
            };

            vm.searchResultsTable = new NgTableParams(initialParameters, settings);
        }

        function mapSearchResults(results) {
            return _.map(results, person => {
                var mappedPerson = {};
                mappedPerson.id = person.id;
                mappedPerson.firstName = person.name;
                mappedPerson.lastName = person.surname;
                mappedPerson.role = person.role.description;
                mappedPerson.team = person.role.team.description;

                return mappedPerson;
            });
        }

        function initDateTimePicker() {
            var options = {
                sideBySide: true,
                locale: 'pl',
                stepping: 30
            };
            $('#custom-filter').datetimepicker(options);
        }

        function queryNotEmpty() {
            return query !== "";
        }

        function applyCustomFilter() {
            var chosenDate = $('#custom-filter').data('DateTimePicker').date();
            console.log('Chosen date:', chosenDate); // TODO API call and refresh table data
        }

        function showRotaForUser(personId) {
            $location.path('/rota/' + personId);
        }

        function isAdmin() {
            return false; // TODO implement permissions checking
        }

        function showActionsForAdmin(personId) {
            alert("Admin settings for: " + personId); // TODO proper implementation (popup or switch view?)
        }
    }
})();
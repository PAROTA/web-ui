(function() {
    'use strict';

    angular
        .module('parota.search')
        .factory('searchService', searchService);

    searchService.$inject = ['$http', 'httpRequestHandlers'];

    function searchService($http, httpRequestHandlers) {
        var service = {
            getData: getData
        };

        return service;
        ////////////////

        function getData(query) {
            return $http.get('/api/search/' + query)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while performing search", shouldDisplayMessage: true}));
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('parota.shifts.all')
        .controller('ShiftsController', ShiftsController);

    ShiftsController.$inject = ['shiftsService', 'constraintsService'];

    function ShiftsController(shiftsService, constraintsService) {
        var vm = this;

        var currentWeekYear = {};

        vm.users = [];
        vm.usersShifts = {};
        vm.dates = {};
        vm.years = [];
        vm.months = [];
        vm.shifts = {};


        vm.getPreviousWeek = getPreviousWeek;
        vm.getNextWeek = getNextWeek;

        init();
        ///////////////////


        function init() {
            currentWeekYear = moment();
            getConstraints();
        }

        function getPreviousWeek() {
            currentWeekYear.subtract(1, 'w');
            getConstraints();
        }

        function getNextWeek(){
            currentWeekYear.add(1, 'w');
            getConstraints();
        }

        function getConstraints() {
            constraintsService.getConstraints().then((response) => {
                loadShifts(response.data);
            });
        }

        function loadShifts(constraints) {
            var opts = {};
            constraints.forEach((o) => {
                opts[o.id.role.id] = opts[o.id.role.id] || {};
                opts[o.id.role.id][o.id.shiftType.id] = {"min" : o.min, "opt" : o.opt};
            });

            shiftsService.getShifts(currentWeekYear.week(), currentWeekYear.year()).then((response) => {
                vm.users = {};
                vm.usersShifts = {};
                vm.dates = {};
                vm.years = [];
                vm.months = [];
                vm.shifts = {};

                response.data.forEach((shift) => {
                    var currentShiftId = shift.shiftId;
                    var currentUser = currentShiftId.user;

                    var currentDate = moment(shift.shiftId.date, 'DD/MM/YYYY');

                    vm.dates[currentDate] = {
                        "day": currentDate.format('DD'),
                        "month": currentDate.format('MMMM'),
                        "year": currentDate.format('YYYY'),
                        "dayOfWeek": currentDate.format('ddd')
                    };

                    vm.users[currentUser.id] = {"userName": currentUser.name, "userSurname": currentUser.surname};
                    vm.usersShifts[currentUser.id] = vm.usersShifts[currentUser.id] || {};
                    vm.usersShifts[currentUser.id][currentDate] = {
                        "shiftTypeId": shift.shiftType.id,
                        "shiftComment": shift.comment
                    };

                    var s = shift.shiftType.id;
                    var r = currentUser.role.id;

                    var o = opts[r][s];
                    if (o === undefined) {
                        o = {"min" : -1, "opt" : -1};
                    }

                    vm.shifts[s] = vm.shifts[s] || {"role" : r, "shift" : s,  "min" : o.min, "opt" : o.opt, "dates" : {}};
                    vm.shifts[s].dates[currentDate] = vm.shifts[s].dates[currentDate] || 0;
                    vm.shifts[s].dates[currentDate] += 1;
                });

                vm.years = countOccurrences(vm.dates, 'year');
                vm.months = countOccurrences(vm.dates, 'month');

                _.forEach(vm.shifts, function (s) {
                    _.forEach(vm.dates, function (key, d) {
                        s.dates[d] = s.dates[d] || 0;
                    });
                });
            });
        }

        function countOccurrences(arr, groupStatement) {
            return _.map(_.groupBy(arr, groupStatement), function (value, key) { return {"value": key, "count" : value.length}; });
        }
    }

})();

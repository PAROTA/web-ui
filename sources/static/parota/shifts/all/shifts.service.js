(function() {
    'use strict';

    angular
        .module('parota.shifts')
        .service('shiftsService', shiftsService);

    shiftsService.$inject = ['$http', 'httpRequestHandlers'];

    function shiftsService($http, httpRequestHandlers) {
        var DEFAULT_PERIOD = 5;

        var service = {
            getShifts: getShifts
        };

        return service;
        ////////////////

        function getShifts (week, year) {
            return $http.get('/api/shifts/' + week + '-' + year + '/' + DEFAULT_PERIOD)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting shifts for all users', shouldDisplayMessage: true}));
        }
    }
})();

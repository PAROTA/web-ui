(function() {
    'use strict';

    angular
        .module('parota.shifts', ['parota.shifts.all',
                                  'parota.shifts.constraints']);

})();
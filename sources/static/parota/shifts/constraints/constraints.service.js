(function() {
    'use strict';

    angular
        .module('parota.shifts.constraints')
        .factory('constraintsService', constraintsService);

    constraintsService.$inject = ['$http', 'httpRequestHandlers'];

    function constraintsService($http, httpRequestHandlers) {
        var BASE_URL = '/api/shifts/constraints';

        var service = {
            getConstraints: getConstraints,
            create: create,
            update: update,
            remove: remove
        };

        return service;
        ////////////////

        function getConstraints() {
            return $http.get(BASE_URL)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting constraints', shouldDisplayMessage: true}));
        }

        function create(constraintData) {
            return $http.post(BASE_URL, constraintData)
                .then(httpRequestHandlers.handleResponse({msg: 'Constraint was created successfully!', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating constraint!', shouldDisplayMessage: true}));
        }

        function update(constraintData) {
            return $http.put(BASE_URL, constraintData)
                .then(httpRequestHandlers.handleResponse({msg: 'Constraint was updated successfully!', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while updating constraint!', shouldDisplayMessage: true}));
        }

        /* The default $http.delete() method doesn't support sending data*/
        function remove(constraintId) {
            return $http({
                method: 'DELETE',
                url: BASE_URL,
                data: constraintId,
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            })
                .then(httpRequestHandlers.handleResponse({msg: 'Constraint was removed successfully!', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while removing constraint', shouldDisplayMessage: true}));
        }
    }
})();
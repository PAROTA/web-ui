(function() {
    'use strict';

    angular
        .module('parota.shifts.constraints')
        .controller('ConstraintsController', ConstraintsController);

    ConstraintsController.$inject = ['allConstraints', 'allRoles', 'allShiftTypes',
                                     'constraintsService', 'NgTableParams'];

    function ConstraintsController(allConstraints, allRoles, allShiftTypes,
                                   constraintsService, NgTableParams) {
        var vm = this;

        vm.constraintsTable = {};
        vm.allRoles = allRoles;
        vm.allShiftTypes = allShiftTypes;

        vm.addConstraint = addConstraint;
        vm.saveConstraint = saveConstraint;
        vm.cancelEditingConstraint = cancelEditingConstraint;
        vm.removeConstraint = removeConstraint;

        init();
        ///////////

        function init() {
            allConstraints.forEach((constraint) => {
                constraint.shiftType = constraint.id.shiftType.id;
                constraint.role = constraint.id.role.id;

                constraint.isEditing = false;
                constraint.isNew = false;
            });

            initConstraintsTable();
        }

        function initConstraintsTable() {
            var initialParameters = {
                count: 9
            };
            var settings = {
                data: angular.copy(allConstraints),
                counts: []
            };

            vm.constraintsTable = new NgTableParams(initialParameters, settings);
        }

        function addConstraint() {
            var newConstraint = {
                shiftType: '',
                role: '',
                workStart: '',
                workEnd: '',
                min: 0,
                opt: 0,
                leave: 0,
                isEdgeShift: false,

                isEditing: true,
                isNew: true
            };

            vm.constraintsTable.settings().data.unshift(newConstraint);
            vm.constraintsTable.reload();
        }

        function saveConstraint(constraint) {
            var constraintData = {
                id: {
                    shiftType: { id: constraint.shiftType },
                    role: { id: constraint.role }
                },
                workStart: constraint.workStart,
                workEnd: constraint.workEnd,
                min: constraint.min,
                opt: constraint.opt,
                leave: constraint.leave,
                isEdgeShift: constraint.isEdgeShift
            };

            console.log(constraint);

            if(constraint.isNew) {

                constraintsService.create(constraintData).then((response) => {
                    constraint.isEditing = false;
                });
            }
            else {
                constraintsService.update(constraintData).then((response) => {
                    constraint.isEditing = false;
                });
            }
        }

        function cancelEditingConstraint(constraint, form) {
            var originalConstraint = resetConstraint(constraint, form);
            angular.extend(constraint, originalConstraint);
        }

        function removeConstraint(constraint) {
            var constraintToRemove = constraint.shiftType + ' ' + constraint.role;
            var response = confirm('Do you want to remove this constraint?\n' + constraintToRemove);
            if (response === true) {
                constraintsService.remove(constraint.id).then((response) => {
                    _.remove(vm.constraintsTable.settings().data, (item) => constraint === item);
                    vm.constraintsTable.reload();
                });
            }
        }

        function resetConstraint(constraint, form) {
            constraint.isEditing = false;
            form.$setPristine();

            return _.head(_.filter(allConstraints, (cst) => cst.shiftType === constraint.shiftType && cst.role === constraint.role));
        }
    }
})();
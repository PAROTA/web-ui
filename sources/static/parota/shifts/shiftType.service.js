(function() {
    'use strict';

    angular
        .module('parota.shifts')
        .factory('shiftTypeService', shiftTypeService);

    shiftTypeService.$inject = ['$http', 'httpRequestHandlers'];

    function shiftTypeService($http, httpRequestHandlers) {
        var service = {
            getAllWorkTypes: getAllWorkTypes
        };

        return service;
        ///////////////

        function getAllWorkTypes() {
            return $http.get('/api/shifts/types/work')
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting shift types', shouldDisplayMessage: true}));
        }
    }
})();
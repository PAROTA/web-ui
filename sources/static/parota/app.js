(function() {
  angular
      .module('parota', ['ngRoute',
                         'ngCookies',
                         'ngMessages',
                         'ui.bootstrap',
                         'parota.navbar',
                         'parota.login',
                         'parota.search',
                         'parota.sidenav',
                         'parota.rota',
                         'parota.shifts',
                         'parota.message',
                         'parota.modals',
                         'parota.team',
                         'parota.user',
                         'parota.bottombar',
                         'parota.notification',
                         'parota.admin'
                        ]);
})();

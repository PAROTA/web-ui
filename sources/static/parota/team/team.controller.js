(function() {
    'use strict';

    angular
        .module('parota.team')
        .controller('TeamController', TeamController);

    TeamController.$inject = ['allTeams', 'NgTableParams', 'modalsService'];

    function TeamController(allTeams, NgTableParams, modalsService) {
        var vm = this;

        var teams = allTeams;

        vm.teamTable = {};

        vm.addTeam = addTeam;
        vm.showTeam = showTeam;
        vm.editTeam = editTeam;
        vm.deleteTeam = deleteTeam;

        init();
        ///////////////

        function init() {
            initTeamTable();
        }

        function initTeamTable() {
            var initialParameters = {
                count: 9
            };
            var settings = {
                data: teams,
                counts: []
            };

            vm.teamTable = new NgTableParams(initialParameters, settings);
        }

        function addTeam() {
            modalsService.open(modalsService.MODALS.ADD_TEAM).result.then((newTeam) => {
                teams.unshift(newTeam);
                vm.teamTable.reload();
            });
        }

        function showTeam(teamId) {
            // TODO redirect to view with users from selected team (?)
        }

        function editTeam(teamToEdit) {
            modalsService.open(modalsService.MODALS.EDIT_TEAM, { teamToEdit: teamToEdit }).result.then((editedTeam) => {
                teams[teams.indexOf(teamToEdit)] = editedTeam;
                vm.teamTable.reload();
            });
        }

        function deleteTeam(teamId) {
            // TODO open "Are you sure?" modal and call delete function from teamService
        }
    }
})();
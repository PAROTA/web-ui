(function() {
    'use strict';

    angular
        .module('parota.team.edit')
        .controller('EditTeamController', EditTeamController);

    EditTeamController.$inject = ['teamToEdit', 'teamService', '$uibModalInstance'];

    function EditTeamController(teamToEdit, teamService, $uibModalInstance) {
        var vm = this;

        vm.teamToEdit = angular.copy(teamToEdit);
        vm.timeZones = [];

        vm.editTeam = editTeam;
        vm.cancel = cancel;

        init();
        /////////////////////////////

        function init() {
            vm.timeZones = moment.tz.names();
        }

        function cancel() {
            $uibModalInstance.dismiss();
        }

        function editTeam() {
            teamService.update(vm.teamToEdit).then((response) => {
                $uibModalInstance.close(vm.teamToEdit);
            });
        }
    }
})();
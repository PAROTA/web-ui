(function() {
    'use strict';

    angular
        .module('parota.team')
        .factory('teamService', teamService);

    teamService.$inject = ['$http', 'httpRequestHandlers'];

    function teamService($http, httpRequestHandlers) {
        var BASE_URL = '/api/teams';

        var service = {
            getAll: getAll,
            create: create,
            update: update,
            remove: remove
        };

        return service;
        ////////////////

        function getAll() {
            return $http.get(BASE_URL)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting all teams', shouldDisplayMessage: true}));
        }

        function create(teamData) {
            return $http.post(BASE_URL, teamData)
                .then(httpRequestHandlers.handleResponse({msg: 'Successfully created new team', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating team', shouldDisplayMessage: true}));
        }

        function update(teamData) {
            return $http.put(BASE_URL, teamData)
                .then(httpRequestHandlers.handleResponse({msg: 'Successfully updated team', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while updating team', shouldDisplayMessage: true}));
        }

        function remove(teamId) {
            return $http.delete(BASE_URL + '/' + teamId)
                .then(httpRequestHandlers.handleResponse({msg: 'Successfully removed team', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while removing team', shouldDisplayMessage: true}));
        }
    }
})();
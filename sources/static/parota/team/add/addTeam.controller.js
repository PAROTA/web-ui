(function() {
    'use strict';

    angular
        .module('parota.team.add')
        .controller('AddTeamController', AddTeamController);

    AddTeamController.$inject = ['teamService', '$uibModalInstance'];

    function AddTeamController(teamService, $uibModalInstance) {
        var vm = this;

        vm.newTeam = {};
        vm.timeZones = [];

        vm.addNewTeam = addNewTeam;
        vm.cancel = cancel;

        init();
        /////////////////////////////

        function init() {
            vm.newTeam = {
                "id" : "",
                "description" : "",
                "zoneId" : ""
            };
            vm.timeZones = moment.tz.names();
        }

        function cancel() {
            $uibModalInstance.dismiss();
        }

        function addNewTeam() {
            teamService.create(vm.newTeam).then((response) => {
                $uibModalInstance.close(response.data);
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('parota.team', ['ngTable',
                                'parota.team.add',
                                'parota.team.edit']);

})();
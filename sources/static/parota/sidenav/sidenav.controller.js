(function() {
    'use strict';

    angular
        .module('parota.sidenav')
        .controller('SidenavController', SidenavController);

    SidenavController.$inject = ['authenticationService', '$location'];

    function SidenavController(authenticationService, $location) {
        var vm = this;

        var SLIDER_WIDTH = '15%',
            SLIDER_BTN_POS = 'Calc(15% - 25px)',
            HIDDEN = '0',
            LEFT_ARROW = 'glyphicon glyphicon-chevron-left',
            RIGHT_ARROW = 'glyphicon glyphicon-chevron-right';

        vm.searchQuery = "";

        vm.isAuthenticated = authenticationService.isAuthenticated;
        vm.search = search;

        init();
        /////////////////

        function init() {
            $('#side-nav-toggle-btn').data('slidedOut', false);

            $('#side-nav-toggle-btn').on('click', function() {
                var isVisible = $(this).data('slidedOut');

                $('#side-nav').css('width', isVisible ? HIDDEN : SLIDER_WIDTH);
                $('.sidenav-button-container').css('margin-left', isVisible ? HIDDEN : SLIDER_BTN_POS);
                $('#side-nav-toggle-btn').attr('class', isVisible ? RIGHT_ARROW : LEFT_ARROW);

                $(this).data('slidedOut', !isVisible);
            });
        }

        function search() {
            if(vm.searchQuery !== "") {
                $location.path('/search/' + vm.searchQuery);
                vm.searchQuery = "";
            }
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('parota.notification')
        .service('notificationService', notificationService);

    notificationService.$inject = ['$http', 'httpRequestHandlers'];

    function notificationService($http, httpRequestHandlers) {

        var SOCKET_STATE = {
            notificationSocketInitialized: false,
            numberOfNewNotifications: 0,
            newNotifications: [],
            listeners: []
        };


        var service = {
            getNotifications : getNotifications,
            getLeaveRequests : getLeaveRequests,
            getChangeRequests : getChangeRequests,
            modifyNotification : modifyNotification,

            initWebSockets: initWebSockets,
            getSocketState: getSocketState,
            clearSocketState: clearSocketState,
            registerListener: registerListener
        };

        return service;
        ////////////////

        function getNotifications() {
            return $http.get('/api/notifications/')
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting notifications", shouldDisplayMessage: true}));
        }

        function getLeaveRequests() {
            return $http.get('/api/notifications/leaveRequest')
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting leave Requests", shouldDisplayMessage: true}));
        }

        function getChangeRequests() {
            return $http.get('/api/notifications/changeRequest')
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting change Requests", shouldDisplayMessage: true}));
        }
        
        function modifyNotification(notification) {
            return $http.put('/api/notifications', notification)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while modifying notification", shouldDisplayMessage: true}));
        }

        function initWebSockets(userId) {
            console.log("init WebSockets: ", userId);

            SOCKET_STATE.notificationSocketInitialized = true;

            var sock = new SockJS('http://' + location.host + '/ws/');
            var stompClient = Stomp.over(sock);

            stompClient.connect('', '', (frame) => {
                stompClient.subscribe('/queue/notifications' + userId, subscribeHandler('[notifications]'));
                stompClient.subscribe('/queue/changeRequests' + userId, subscribeHandler('[changeRequests]'));
                stompClient.subscribe('/queue/leaveRequests' + userId, subscribeHandler('[leaveRequests]'));
            });


            function subscribeHandler(type) {
                return (message) => {
                    console.log(type + ' Received message:', message);
                    SOCKET_STATE.newNotifications.push(message.data);
                    SOCKET_STATE.numberOfNewNotifications++;
                    SOCKET_STATE.listeners.forEach(listener => listener.call());
                };
            }
        }

        function getSocketState() {
            return SOCKET_STATE;
        }

        function clearSocketState() {
            SOCKET_STATE.newNotifications = [];
            SOCKET_STATE.numberOfNewNotifications = 0;
        }

        function registerListener(listener) {
            SOCKET_STATE.listeners.push(listener);
        }
    }
})();

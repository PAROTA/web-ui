(function() {
    'use strict';

    angular
        .module('parota.notification.all')
        .controller('AllNotificationsController', AllNotificationsController);

    AllNotificationsController.$inject = ['targetUser', 'notificationService'];

    function AllNotificationsController(targetUser, notificationService) {
        var vm = this;

        vm.user = targetUser;

        vm.notificationsAvailable = false;
        vm.changeRequestsAvailable = false;
        vm.leaveRequestsAvailable = false;

        vm.notifications = [];
        vm.changeRequests = [];
        vm.leaveRequests = [];

        vm.readNotification = readNotification;
        vm.acceptRequest = acceptRequest;

        init();
        ///////////////////


        function init() {
            notificationService.getNotifications().then((response) => {
                vm.notifications = response.data;
                vm.notificationsAvailable = true;
            });
            notificationService.getLeaveRequests().then((response) => {
                vm.leaveRequests = response.data;
                vm.leaveRequestsAvailable = true;
            });
            notificationService.getChangeRequests().then((response) => {
                vm.changeRequests = response.data;
                vm.changeRequestsAvailable = true;
            });
        }
        
        function readNotification(notification) {
            notification.status = "R";
            notificationService.modifyNotification(notification);
        }

        function acceptRequest(request) {
            if(request.notification.status!=="A" && request.sender.id!==vm.user.id) {
                var r = request.notification.message + "\nFrom: " + request.sender.name + " " + request.sender.surname;
                var response = confirm("Do you want to accept this request?\n" + r);
                if (response === true) {
                    request.notification.status = "A";
                    notificationService.modifyNotification(request.notification);
                }
            }
        }

    }

})();

(function() {
    'use strict';

    angular
        .module('parota.notification', ['parota.notification.all',
                                        'parota.notification.leaveRequest',
                                        'parota.notification.changeRequest']);
})();
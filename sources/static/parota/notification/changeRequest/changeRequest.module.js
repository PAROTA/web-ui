(function() {
    'use strict';

    angular
        .module('parota.notification.changeRequest', ['parota.notification.changeRequest.add']);

})();
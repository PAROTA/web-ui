(function() {
    'use strict';

    angular
        .module('parota.notification.changeRequest')
        .factory('changeRequestService', changeRequestService);

    changeRequestService.$inject = ['$http', 'httpRequestHandlers'];

    function changeRequestService($http, httpRequestHandlers) {
        var service = {
            create: create
        };

        return service;
        ////////////////

        function create(changeRequestData) {
            return $http.post('/api/notifications/changeRequest', changeRequestData)
                .then(httpRequestHandlers.handleResponse({msg: 'Change request was submitted successfully!', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating change request', shouldDisplayMessage: true}));
        }
    }
})();
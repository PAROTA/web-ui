(function() {
    'use strict';

    angular
        .module('parota.notification.changeRequest.add')
        .controller('AddChangeRequestController', AddChangeRequestController);

    AddChangeRequestController.$inject = ['changeRequestService', 'authenticationService',
                                          '$uibModalInstance', '$timeout', 'targetUser'];

    function AddChangeRequestController(changeRequestService, authenticationService,
                                        $uibModalInstance, $timeout, targetUser) {
        var vm = this;

        var START_DATE_PICKER = '#startDate';
        var END_DATE_PICKER = '#endDate';

        vm.currentUser = {};
        vm.targetUser = {};
        vm.message = "";

        vm.addNewChangeRequest = addNewChangeRequest;
        vm.cancel = cancel;

        init();
        /////////

        function init() {
            initUsers();
            $timeout(() => initDatePickers()); // $timeout() used because template must be rendered before initializing datepickers
        }

        function initUsers() {
            vm.currentUser = authenticationService.getUserData();
            vm.targetUser = targetUser;

            vm.currentUser.displayName = vm.currentUser.name + ' ' + vm.currentUser.surname;
            vm.targetUser.displayName = targetUser.name + ' ' + targetUser.surname;
        }

        function initDatePickers() {
            var options = {
                locale: 'pl',
                format: 'DD/MM/YYYY'
            };

            $(START_DATE_PICKER).datetimepicker(options).on('dp.change', updateMinDateForSecondPicker);
            $(END_DATE_PICKER).datetimepicker(options);

            function updateMinDateForSecondPicker() {
                var minDate = getDateFrom(START_DATE_PICKER);
                if(minDate) {
                    $(END_DATE_PICKER).data('DateTimePicker').minDate(minDate);
                }
            }
        }

        function getDateFrom(inputId) {
            return $(inputId).data('DateTimePicker').date();
        }

        function addNewChangeRequest() {
            var changeRequestData = {};
            changeRequestData.shiftStart = getDateFrom(START_DATE_PICKER).format('DD/MM/YYYY');
            changeRequestData.shiftEnd = getDateFrom(END_DATE_PICKER).format('DD/MM/YYYY');
            changeRequestData.sender = { id: vm.currentUser.id };
            changeRequestData.notification = {
                receiver: { id: vm.targetUser.id },
                message: vm.message
            };

            changeRequestService.create(changeRequestData).then((response) => {
                $uibModalInstance.close();
            });
        }

        function cancel() {
            $uibModalInstance.close();
        }
    }
})();
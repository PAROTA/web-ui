(function() {
    'use strict';

    angular
        .module('parota.notification.leaveRequest.add')
        .controller('AddLeaveRequestController', AddLeaveRequestController);

    AddLeaveRequestController.$inject = ['leaveRequestService', 'authenticationService', '$uibModalInstance', '$timeout'];

    function AddLeaveRequestController(leaveRequestService, authenticationService, $uibModalInstance, $timeout) {
        var vm = this;

        var START_DATE_PICKER = '#startDate';
        var END_DATE_PICKER = '#endDate';

        vm.leaveTypes = [];

        vm.leaveType = {};
        vm.message = "";

        vm.addNewLeaveRequest = addNewLeaveRequest;
        vm.cancel = cancel;

        init();
        ////////////////

        function init() {
            getAllLeaveRequestTypes();
            $timeout(() => initDatePickers()); // $timeout() used because template must be rendered before initializing datepickers
        }

        function getAllLeaveRequestTypes() {
            leaveRequestService.getLeaveRequestTypes().then((response) => {
                vm.leaveTypes = response.data;
            });
        }

        function initDatePickers() {
            var options = {
                locale: 'pl',
                format: 'DD/MM/YYYY'
            };

            $(START_DATE_PICKER).datetimepicker(options).on('dp.change', updateMinDateForSecondPicker);
            $(END_DATE_PICKER).datetimepicker(options);

            function updateMinDateForSecondPicker() {
                var minDate = getDateFrom(START_DATE_PICKER);
                if(minDate) {
                    $(END_DATE_PICKER).data('DateTimePicker').minDate(minDate);
                }
            }
        }

        function getDateFrom(inputId) {
            return $(inputId).data('DateTimePicker').date();
        }

        function addNewLeaveRequest() {
            var leaveRequestData = {};
            leaveRequestData.leaveStart = getDateFrom(START_DATE_PICKER).format('DD/MM/YYYY');
            leaveRequestData.leaveEnd = getDateFrom(END_DATE_PICKER).format('DD/MM/YYYY');
            leaveRequestData.leaveType = { id: vm.leaveType };
            leaveRequestData.sender = { id: authenticationService.getUserData().id };
            leaveRequestData.notification = {
                message: vm.message,
                receiver: { id: null }
            };

            leaveRequestService.create(leaveRequestData).then((response) => {
                $uibModalInstance.close();
            });
        }

        function cancel() {
            $uibModalInstance.dismiss();
        }
    }
})();
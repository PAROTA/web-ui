(function() {
    'use strict';

    angular
        .module('parota.notification.leaveRequest', ['parota.notification.leaveRequest.add']);

})();
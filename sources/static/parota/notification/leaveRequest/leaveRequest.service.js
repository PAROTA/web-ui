(function() {
    'use strict';

    angular
        .module('parota.notification.leaveRequest')
        .factory('leaveRequestService', leaveRequestService);

    leaveRequestService.$inject = ['$http', 'httpRequestHandlers'];

    function leaveRequestService($http, httpRequestHandlers) {
        var service = {
            getLeaveRequestTypes: getLeaveRequestTypes,
            create: create
        };

        return service;
        ///////////////

        function getLeaveRequestTypes() {
            return $http.get('/api/shifts/types/leave')
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting leave request types', shouldDisplayMessage: true}));
        }

        function create(leaveRequestData) {
            return $http.post('/api/notifications/leaveRequest', leaveRequestData)
                .then(httpRequestHandlers.handleResponse({msg: 'Leave request was submitted successfully!', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating leave request', shouldDisplayMessage: true}));
        }
    }
})();
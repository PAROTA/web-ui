(function() {
    'use strict';

    angular
        .module('parota.navbar')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$scope', '$location', 'authenticationService', 'notificationService'];

    function NavbarController($scope, $location, authenticationService, notificationService) {
        var vm = this;

        vm.socketState = notificationService.getSocketState;

        vm.isAuthenticated = authenticationService.isAuthenticated;
        vm.userData = authenticationService.getUserData;
        vm.goToHomepage = goToHomepage;
        vm.goToLoginPage = goToLoginPage;
        vm.goToNotifications = goToNotifications;
        vm.logout = logout;

        init();
        //////////////

        function init() {
            notificationService.registerListener(() => $scope.$apply());
        }

        function goToHomepage() {
            if(authenticationService.isAuthenticated()) {
                $location.path('/rota');
            }
            else {
                $location.path('/');
            }
        }

        function goToLoginPage() {
            $location.path('/login');
        }

        function goToNotifications() {
            $location.path('/notifications');
            notificationService.clearSocketState();
        }

        function logout() {
            authenticationService.logout().then(() => {
                authenticationService.setUserData({});
                $location.path('/');
            });
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('parota.user')
        .factory('userTypeService', userTypeService);

    userTypeService.$inject = ['$http', 'httpRequestHandlers'];

    function userTypeService($http, httpRequestHandlers) {
        var BASE_URL = '/api/users/types';

        var service = {
            getAll: getAll,
            getById: getById,
            create: create,
            update: update,
            remove: remove
        };

        return service;
        ///////////////

        function getAll() {
            return $http.get(BASE_URL)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting all user types', shouldDisplayMessage: true}));
        }

        function getById(userTypeId) {
            return $http.get(BASE_URL + '/' + userTypeId)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting user type with id = ' + userTypeId, shouldDisplayMessage: true}));
        }

        function create(userTypeData) {
            return $http.post(BASE_URL, userTypeData)
                .then(httpRequestHandlers.handleResponse({msg: 'User was created successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating user type', shouldDisplayMessage: true}));
        }

        function update(userTypeData) {
            return $http.put(BASE_URL, userTypeData)
                .then(httpRequestHandlers.handleResponse({msg: 'User was updated successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while updating user type', shouldDisplayMessage: true}));
        }

        function remove(userTypeId) {
            return $http.delete(BASE_URL + '/' + userTypeId)
                .then(httpRequestHandlers.handleResponse({msg: 'User was removed successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while removing user type with id = ' + userTypeId, shouldDisplayMessage: true}));
        }
    }
})();
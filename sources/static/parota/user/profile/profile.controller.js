(function() {
    'use strict';

    angular
        .module('parota.user.profile')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['user', 'modalsService'];

    function ProfileController(user, modalsService) {
        var vm = this;

        vm.user = {};
        
        vm.changePassword = changePassword;
        vm.changeTimeZone = changeTimeZone;
        vm.changeAvatar = changeAvatar;
        vm.newLeaveRequest = newLeaveRequest;

        init();
        ///////////////

        function init() {
            vm.user = user;
        }

        function changePassword() {
            //TODO implement this in the next phase
        }
        
        function changeTimeZone() {
            //TODO implement this in the next phase
        }
        
        function changeAvatar() {
            //TODO implement this in the next phase
        }

        function newLeaveRequest() {
            modalsService.open(modalsService.MODALS.ADD_LEAVE_REQUEST);
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('parota.user.remove')
        .controller('UserRemoveController', UserRemoveController);

    UserRemoveController.$inject = ['userService', '$timeout', '$uibModalInstance', 'userToRemove'];

    function UserRemoveController(userService, $timeout, $uibModalInstance, userToRemove) {
        var vm = this;

        vm.userToRemove = userToRemove;

        vm.removeUser = removeUser;
        vm.cancel = cancel;

        init();
        ////////////

        function init() {
            $timeout(() => initDatePicker());
        }

        function initDatePicker() {
            var options = {
                locale: 'pl',
                format: 'DD/MM/YYYY',
                defaultDate: moment()
            };

            $('#user-remove-date').datetimepicker(options);
        }

        function removeUser() {
            var removeDate = $('#user-remove-date').data('DateTimePicker').date();
            userService.remove(userToRemove.id, removeDate.format('DD-MM-YYYY')).then((response) => {
                $uibModalInstance.close(true);
            });
        }

        function cancel() {
            $uibModalInstance.dismiss();
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('parota.user.edit')
        .controller('EditUserController', EditUserController);

    EditUserController.$inject = ['userService', 'userTypeService', 'userRoleService', 'teamService',
        'userToEdit', '$uibModalInstance'];

    function EditUserController(userService, userTypeService, userRoleService, teamService,
                               userToEdit, $uibModalInstance) {
        var vm = this;

        vm.teams = [];
        vm.roles = [];
        vm.userTypes = [];

        vm.userToEdit = userToEdit;

        vm.filterRolesForTeam = filterRolesForTeam;
        vm.editUser = editUser;
        vm.cancel = cancel;

        init();
        /////////////////////////////

        function init() {
            userTypeService.getAll().then((response) => {
                vm.userTypes = response.data;
            });

            userRoleService.getAll().then((response) => {
                vm.roles = response.data;
            });

            teamService.getAll().then((response) => {
                vm.teams = response.data;
            });
        }

        function cancel() {
            $uibModalInstance.dismiss();
        }

        function editUser() {
            userService.update(vm.userToEdit).then((response) => {
                init();
                $uibModalInstance.close(vm.userToEdit);
            });
        }

        function filterRolesForTeam(teamId) {
            return _.filter(vm.roles, role => role.team.id === teamId);
        }
    }
})();

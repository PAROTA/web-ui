(function() {
    'use strict';

    angular
        .module('parota.user')
        .factory('userRoleService', userRoleService);

    userRoleService.$inject = ['$http', 'httpRequestHandlers'];

    function userRoleService($http, httpRequestHandlers) {
        var BASE_URL = '/api/roles';

        var service = {
            getAll: getAll,
            getById: getById,
            create: create,
            update: update,
            remove: remove
        };

        return service;
        ///////////////

        function getAll() {
            return $http.get(BASE_URL)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting all user types', shouldDisplayMessage: true}));
        }

        function getById(roleId) {
            return $http.get(BASE_URL + '/' + roleId)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting user type with id = ' + roleId, shouldDisplayMessage: true}));
        }

        function create(roleData) {
            return $http.post(BASE_URL, roleData)
                .then(httpRequestHandlers.handleResponse({msg: 'User was created successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating user type', shouldDisplayMessage: true}));
        }

        function update(roleData) {
            return $http.put(BASE_URL, roleData)
                .then(httpRequestHandlers.handleResponse({msg: 'User was updated successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while updating user type', shouldDisplayMessage: true}));
        }

        function remove(roleId) {
            return $http.delete(BASE_URL + '/' + roleId)
                .then(httpRequestHandlers.handleResponse({msg: 'User was removed successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while removing user type with id = ' + roleId, shouldDisplayMessage: true}));
        }
    }
})();
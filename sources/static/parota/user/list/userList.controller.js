(function() {
    'use strict';

    angular
        .module('parota.user.list')
        .controller('UserListController', UserListController);

    UserListController.$inject = ['allUsers', 'NgTableParams', 'modalsService', '$location', 'userService'];

    function UserListController(allUsers, NgTableParams, modalsService, $location, userService) {
        var vm = this;

        var users = allUsers;

        vm.userTable = {};

        vm.addUser = addUser;
        vm.showUser = showUser;
        vm.editUser = editUser;
        vm.removeUser = removeUser;

        init();
        ///////////////

        function init() {
            initUserTable();
        }

        function initUserTable() {
            var initialParameters = {
                count: 9
            };
            var settings = {
                data: users,
                counts: []
            };

            vm.userTable = new NgTableParams(initialParameters, settings);
        }

        function addUser() {
            modalsService.open(modalsService.MODALS.ADD_USER).result.then((response) => {
                users.unshift(response);
                vm.userTable.reload();
            });
        }

        function showUser(userId) {
            $location.path('/profile/' + userId);
        }

        function editUser(user) {
            userService.getById(user.id).then((response) => {
                modalsService.open(modalsService.MODALS.EDIT_USER, { userToEdit : response.data }).result.then((editedUser) => {
                    users[users.indexOf(user)] = editedUser;
                    vm.userTable.reload();
                });
            });
        }

        function removeUser(user) {
            modalsService.open(modalsService.MODALS.REMOVE_USER, { userToRemove: user }).result.then((wasUserRemoved) => {
                if(wasUserRemoved) {
                    users[users.indexOf(user)].status = "DELETED";
                    vm.userTable.reload();
                }
            });
        }
    }
})();
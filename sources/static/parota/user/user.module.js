(function() {
    'use strict';

    angular
        .module('parota.user', ['parota.user.add',
                                'parota.user.edit',
                                'parota.user.list',
                                'parota.user.profile',
                                'parota.user.remove']);

})();
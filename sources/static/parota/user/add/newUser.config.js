(function() {
    'use strict';

    angular
        .module('parota.user.add')
        .constant('newUserConfig', newUserConfig);

    function newUserConfig() {
            return {
                pid: "",
                name: "",
                surname: "",
                email: "",
                role: "",
                team: "",
                userType: ""
            };
        }
})();

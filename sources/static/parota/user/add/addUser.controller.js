(function() {
    'use strict';

    angular
        .module('parota.user.add')
        .controller('AddUserController', AddUserController);

    AddUserController.$inject = ['userService', 'userTypeService', 'userRoleService', 'teamService',
                                 'newUserConfig', '$uibModalInstance'];

    function AddUserController(userService, userTypeService, userRoleService, teamService,
                               newUserConfig, $uibModalInstance) {
        var vm = this;

        vm.teams = [];
        vm.roles = [];
        vm.userTypes = [];

        vm.newUser = {};

        vm.filterRolesForTeam = filterRolesForTeam;
        vm.addNewUser = addNewUser;
        vm.cancel = cancel;

        init();
        /////////////////////////////

        function init() {
            vm.newUser = newUserConfig();

            userTypeService.getAll().then((response) => {
                vm.userTypes = response.data;
            });

            userRoleService.getAll().then((response) => {
                vm.roles = response.data;
            });

            teamService.getAll().then((response) => {
                vm.teams = response.data;
            });
        }

        function cancel() {
            $uibModalInstance.dismiss();
        }

        function addNewUser() {
            var newUser = {};
            newUser.pid = vm.newUser.pid;
            newUser.name =  vm.newUser.name;
            newUser.surname = vm.newUser.surname;
            newUser.email = vm.newUser.email;
            newUser.password = "changeme"; // TODO remember to remove it when auto. passwords generation will work
            newUser.role = { "id" : vm.newUser.role };
            newUser.userType = { "id" : vm.newUser.userType };

            userService.create(newUser).then((response) => {
                init();
                $uibModalInstance.close(response.data);
            });
        }

        function filterRolesForTeam(teamId) {
            return _.filter(vm.roles, role => role.team.id === teamId);
        }
    }
})();

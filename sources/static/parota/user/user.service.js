(function() {
    'use strict';

    angular
        .module('parota.user')
        .factory('userService', userService);

    userService.$inject = ['$http', 'httpRequestHandlers'];

    function userService($http, httpRequestHandlers) {
        var BASE_URL = '/api/users';

        var service = {
            getAll: getAll,
            getById: getById,
            create: create,
            update: update,
            remove: remove
        };

        return service;
        ////////////////

        function getAll() {
            return $http.get(BASE_URL)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting all users', shouldDisplayMessage: true}));
        }

        function getById(userId) {
            return $http.get(BASE_URL + '/' + userId)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while getting user with id = ' + userId, shouldDisplayMessage: true}));
        }

        function create(userData) {
            return $http.post(BASE_URL, userData)
                .then(httpRequestHandlers.handleResponse({msg: 'User was created successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating user', shouldDisplayMessage: true}));
        }

        function update(userData) {
            return $http.put(BASE_URL, userData)
                .then(httpRequestHandlers.handleResponse({msg: 'User was updated successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while updating user', shouldDisplayMessage: true}));
        }

        function remove(userId, removeDate) {
            return $http.delete(BASE_URL + '/' + userId + '/' + removeDate)
                .then(httpRequestHandlers.handleResponse({msg: 'User was removed successfully', shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: 'An error occurred while removing user with id = ' + userId, shouldDisplayMessage: true}));
        }
    }
})();
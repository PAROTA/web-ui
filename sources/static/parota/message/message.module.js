(function() {
    'use strict';

    angular
        .module('parota.message', ['angular-growl']);
})();
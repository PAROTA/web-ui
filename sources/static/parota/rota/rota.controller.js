(function() {
    'use strict';

    angular
        .module('parota.rota')
        .controller('RotaController', RotaController);

    RotaController.$inject = ['rotaService', 'authenticationService', 'modalsService', 'targetUser'];

    function RotaController(rotaService, authenticationService, modalsService, targetUser) {
        var vm = this;

        var currentWeekYear = {};

        vm.user = targetUser;
        vm.shifts = [];
        vm.years = [];
        vm.months = [];

        vm.getPreviousWeek = getPreviousWeek;
        vm.getNextWeek = getNextWeek;
        vm.shouldShowChangeRequestButton = shouldShowChangeRequestButton;
        vm.newChangeRequest = newChangeRequest;

        init();
        ///////////////////


        function init() {
            currentWeekYear = moment();
            loadShifts();
        }

        function getPreviousWeek() {
            currentWeekYear.subtract(1, 'w');
            loadShifts();
        }

        function getNextWeek(){
            currentWeekYear.add(1, 'w');
            loadShifts();
        }

        function loadShifts() {
            rotaService.getShifts(vm.user.id, currentWeekYear.week(), currentWeekYear.year()).then((response) => {
                vm.shifts = response.data;
                vm.shifts.forEach((shift) => {
                    var currentDate = moment(shift.shiftId.date, 'DD/MM/YYYY');

                    shift.shiftId.day = currentDate.format('DD');
                    shift.shiftId.month = currentDate.format('MMMM');
                    shift.shiftId.year = currentDate.format('YYYY');
                    shift.shiftId.dayOfWeek = currentDate.format('ddd');
                });

                vm.years = countOccurrences(vm.shifts, 'shiftId.year');
                vm.months = countOccurrences(vm.shifts, 'shiftId.month');
            });
        }

        function countOccurrences(arr, groupStatement) {
            return _.map(_.groupBy(arr, groupStatement), function (value, key) { return {"value": key, "count" : value.length}; });
        }

        function shouldShowChangeRequestButton() {
            return authenticationService.getUserData().id !== targetUser.id;
        }

        function newChangeRequest() {
            modalsService.open(modalsService.MODALS.ADD_CHANGE_REQUEST, {targetUser: targetUser});
        }
    }

})();

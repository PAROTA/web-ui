(function() {
    'use strict';

    angular
        .module('parota.rota')
        .service('rotaService', rotaService);

    rotaService.$inject = ['$http', 'httpRequestHandlers'];

    function rotaService($http, httpRequestHandlers) {
        var DEFAULT_PERIOD = 5;

        var service = {
            getShifts: getShifts
        };

        return service;
        ////////////////

        function getShifts (userId, week, year) {
            return $http.get('/api/shifts/' + userId + '/' + week + '-' + year + '/' + DEFAULT_PERIOD)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting shift for certain user", shouldDisplayMessage: true}));
        }
    }
})();

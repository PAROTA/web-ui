(function() {
    'use strict';

    angular
        .module('parota.modals')
        .factory('modalsService', modalsService);

    modalsService.$inject = ['$uibModal'];

    function modalsService($uibModal) {
        var MODALS = {
            ADD_USER: {
                template: '/static/parota/user/add/addUser.view.html',
                controller: 'AddUserController'
            },
            EDIT_USER: {
                template: '/static/parota/user/edit/editUser.view.html',
                controller: 'EditUserController'
            },
            REMOVE_USER: {
                template: '/static/parota/user/remove/userRemove.view.html',
                controller: 'UserRemoveController'
            },
            ADD_LEAVE_REQUEST: {
                template: '/static/parota/notification/leaveRequest/add/addLeaveRequest.view.html',
                controller: 'AddLeaveRequestController'
            },
            ADD_CHANGE_REQUEST: {
                template: '/static/parota/notification/changeRequest/add/addChangeRequest.view.html',
                controller: 'AddChangeRequestController'
            },
            ADD_TEAM: {
                template: '/static/parota/team/add/addTeam.view.html',
                controller: 'AddTeamController'
            },
            EDIT_TEAM: {
                template: '/static/parota/team/edit/editTeam.view.html',
                controller: 'EditTeamController'
            }
        };

        var service = {
            MODALS: MODALS,
            open: open
        };

        return service;
        ///////////////

        function open(modalName, resolveData = {}) {
            return $uibModal.open({
                templateUrl: modalName.template,
                controller: modalName.controller,
                controllerAs: 'vm',
                backdrop: 'static',
                resolve: resolveData
            });
        }
    }
})();